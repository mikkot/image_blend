package com.mikkot.image_blend;
import java.awt.image.BufferedImage;

import javafx.embed.swing.SwingFXUtils;
import javafx.stage.Stage;

public class ImageBlend {
    public static void main(String[] args) {
        ImageBlend app = new ImageBlend();
        app.handleArgs(args);
    }

    public void handleArgs(String[] args) {
        if(args == null || args.length == 0) {
            error("No arguments given.");
            printHelp();
        }

        if(args.length > 1) {
            // Currently only filepath argument is supported
            error("Too many arguments given!");
            printHelp();
        }

        BufferedImage image = null;
        for(String argument : args) {
            if(argument.startsWith("-") || argument.startsWith("--")) {
                // Option flag. Not implemented yet.
                error("Flags are not supported yet.");
                printHelp();
                return;
            }

            // First argument is the image path and if no image is supplied we
            // can not run the program, so we can just check if our image is null
            // at each argument.
            FileHandler fileHandler = new FileHandler();
            if (image == null) {
                image = fileHandler.loadImageFromPath(argument);
            }
            if(image == null) {
                error("Could not load image from path: '" + argument + "'");
                return;
            }


            switch(argument) {
                case "blend":
                    Blender blender = new Blender();
                    BufferedImage mirrored = blender.mirrorImageHorizontally(image);

                    fileHandler.saveImageAsCopy(
                        blender.combineImages(image, mirrored),
                        argument
                    );
                    break;
                case "edit":
                    Editor editor = new Editor(SwingFXUtils.toFXImage(image, null));
                    editor.start(new Stage());
                    break;
                default:
                    break;
            }
            
            // Currently only 2 arguments are supported, so after loading the
            // image and handling the second arguments, we can break our argu-
            // ment handler loop here.
            break;
        }
    }

    public void error(String message) {
        System.out.println("Error: " + message);
    }

    public void printHelp() {
        System.out.println("Usage: \nImageBlend <pathToFile>");
        System.out.println("Supported file extensions: \n\t.jpg\n\t.jpeg\n\t.png");
    }
}