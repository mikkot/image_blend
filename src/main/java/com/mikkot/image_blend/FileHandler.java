package com.mikkot.image_blend;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class FileHandler {
    public FileHandler() {}

    public BufferedImage loadImageFromPath(String path) {
        if(!(path.endsWith(".jpg") || !path.endsWith(".jpeg") || !path.endsWith(".png"))) {
            return null;
        }
        System.out.println("Loading image...");
        return readImage(path);
    }

    public void saveImageAsCopy(BufferedImage image, String originalPath) {
        // For now we assume there is only one dot in the filename
        System.out.println("Saving modified image: '" + originalPath + "''...");
        String imageNameWithoutFileExtension = originalPath.split("\\.")[0];
        File newImage = new File(imageNameWithoutFileExtension + "_ copy.png");
        try {
            ImageIO.write(image, "png", newImage);
        } catch(Exception error) {
            System.out.println("Unable to save image!\n" + error.getMessage());
            return;
        }

        System.out.println("Modified image saved to: '" + imageNameWithoutFileExtension + "'");
    }

    private BufferedImage readImage(String path) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(path));
        } catch (Exception e) {
            System.err.println("Could not load image from path: '" + path + "'");
            return null;
        }

        System.out.println("Image loaded!");
        return image;
    }


}
