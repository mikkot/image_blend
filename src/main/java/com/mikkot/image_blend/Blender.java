package com.mikkot.image_blend;
import java.awt.image.BufferedImage;
import java.awt.Color;

public class Blender {
    public Blender() {}

    public BufferedImage blendImageEdges(BufferedImage image) {
        // 1. Change pixels on the RIGHT edge of the image closer to pixels on
        // the LEFT edge with corresponding Y coordinates
        int widthInPixels = 200;

        for(int xPos = 0; xPos<widthInPixels; xPos++) {
            for(int yPos = 0; yPos<image.getHeight(); yPos++) {
                image.setRGB(xPos, yPos, Color.GRAY.getRGB());
                // image.setRGB(image.getWidth() - xPos, image.getHeight() - yPos, randomizePixel(image.getRGB(x, y)));
            }
        }

        return image;
    }

    private int randomizePixel() {
        return 0;
    }

    public BufferedImage mirrorImageHorizontally(BufferedImage image) {
        System.out.println("Creating a mirrored copy of image...");
        BufferedImage mirrored = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        for(int x = 0; x<image.getWidth(); x++) {
            for(int y = 0; y<image.getHeight(); y++) {
                mirrored.setRGB(image.getWidth() - 1 - x, y, image.getRGB(x, y));
            }
        }

        System.out.println("Mirrored copy ready!");
        return mirrored;
    }

    public BufferedImage combineImages(BufferedImage imageA, BufferedImage imageB) {
        System.out.println("Combining the two images...");
        BufferedImage combined = new BufferedImage(
            imageA.getWidth() + imageB.getWidth(),
            imageA.getHeight(),
            imageA.getType()
        );

        System.out.println("Printing imageA into new image...");
        for(int x = 0; x<imageA.getWidth(); x++) {
            for(int y = 0; y<imageA.getHeight(); y++) {
                combined.setRGB(x, y, imageA.getRGB(x, y));
            }
        }
        System.out.println("ImageA printed!");

        System.out.println("Printing imageB into new image...");
        for(int x = 0; x<imageB.getWidth(); x++) {
            for(int y = 0; y<imageB.getHeight(); y++) {
                combined.setRGB(x + imageA.getWidth() - 1, y, imageB.getRGB(x, y));
            }
        }
        System.out.println("ImageB printed!");


        System.out.println("Images combined!");
        return combined;
    }
}
