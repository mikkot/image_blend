package com.mikkot.image_blend;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Editor extends Application {
  private Image image;

  public Editor() {}
  public Editor(Image image) {
    this.setImage(image);
  }

  @Override
  public void start(Stage stage) {
    stage.setTitle("Edit image");
    stage.setWidth(1366);
    stage.setHeight(768);

    Scene scene = new Scene(new Group());
    VBox root = new VBox();

    ImageView currentImage = new ImageView();
    if(this.image != null) {
      currentImage.setImage(this.getImage());
      root.getChildren().addAll(currentImage);
      scene.setRoot(root);
      stage.setScene(scene);
      stage.show();
    } else {
      System.out.println("No image specified.");
    }
  }

  public void setImage(Image image) {
    this.image = image;
  }
  public Image getImage() {
    return this.image;
  }
}
