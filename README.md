# ImageBlend

### Usage

The application takes two arguments:

1) Path to an image file (in JPG, JPEG or PNG format)
2) Action on the image. Currently 'blend' or 'edit'

'blend' will copy the image, mirror it horizontally, and then glue those images
together and save it next to the original.

'edit' will open the image in a new window.
